#include "setting.h"
#include "ff.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"


int8_t properties_add(container_prop_t *c_prop, char* _key, size_t size_key, char* _value, size_t size_value)
{
	properties_t *prop = malloc(sizeof(properties_t));
	properties_t **prop_tmp;
	
	prop->key = malloc((size_key + 1) * sizeof(char));
	if(prop->key == NULL)
	{
		free(prop);
		return 0;
	}
    memset(prop->key, 0, (size_key + 1) * sizeof(char));
	memcpy(prop->key, _key, size_key);
	prop->value = malloc((size_value + 1) * sizeof(char));
	if(prop->value == NULL)
	{
		free(prop);
		free(prop->key);
		return 0;
	}
    memset(prop->value, 0, (size_value + 1) * sizeof(char));
	memcpy(prop->value, _value, size_value);
	prop->size_key = size_key;
	prop->size_value = size_value;
	
	if(prop == NULL)
		return -1;
	if(c_prop->properties == NULL)
	{
		prop_tmp = malloc(sizeof(properties_t*));
		if(prop_tmp == NULL) 
		{
			free(prop);
			free(prop->key);
			free(prop->value);
			return -1;
		}
		c_prop->properties = prop_tmp;
	}
	else
	{
		prop_tmp = realloc(c_prop->properties, sizeof(properties_t*) * (c_prop->size + 1));
		if(prop_tmp == NULL) 
		{
			free(prop);
			free(prop->key);
			free(prop->value);
			return -1;
		}
		c_prop->properties = prop_tmp;
	}
	
	c_prop->properties[c_prop->size++] = prop;
	
	return 0;
}

int8_t setting_get_properties(container_prop_t *c_prop, const char *key, char* value)
{
	for(uint8_t i = 0; i < c_prop->size; i++)
	{
		if(memcmp(key, c_prop->properties[i]->key, strlen(key)))
			continue;
		memcpy(value, c_prop->properties[i]->value, c_prop->properties[i]->size_value);
		return 0;
	}
	return -1;
}

int8_t setting_get_properties_to_int(container_prop_t *c_prop, const char *key, uint16_t *value)
{
	char _buff[50];
	for(uint8_t i = 0; i < c_prop->size; i++)
	{
		if(memcmp(key, c_prop->properties[i]->key, strlen(key)))
			continue;
		memset(_buff, 0, sizeof(_buff));
		memcpy(_buff, c_prop->properties[i]->value, c_prop->properties[i]->size_value);
		sscanf(_buff, "%i", value);
		return 0;
	}
	return -1;
}

int8_t setting_set_properties_u16(container_prop_t *c_prop, const char *key, uint16_t value)
{
	char _buff[50];
	char * tmp;
	for(uint8_t i = 0; i < c_prop->size; i++)
	{
		if(memcmp(key, c_prop->properties[i]->key, strlen(key)))
			continue;
		memset(_buff, 0, sizeof(_buff));
		sprintf(_buff, "%i", value);
		tmp = malloc(strlen(_buff) + 1);
		if(tmp == NULL)
			return -1;
        memset(tmp, 0, sizeof(strlen(_buff)) + 1);
		free(c_prop->properties[i]->value);
		c_prop->properties[i]->value = tmp;
		c_prop->properties[i]->size_value = strlen(_buff);
		memcpy(c_prop->properties[i]->value, _buff, c_prop->properties[i]->size_value);
		return 0;
	}
    memset(_buff, 0, sizeof(_buff));
    sprintf(_buff, "%i", value);
    properties_add(c_prop, (char*)key, strlen(key), _buff, strlen(_buff));
	return -1;
}

int8_t setting_load(container_prop_t *c_prop, const char* path)
{
	FRESULT result;
	FIL fsrc;
    static char *buff;
    char buffer[100];
    char str1[50], str2[50];
	
    result = f_open(&fsrc, path, FA_OPEN_ALWAYS | FA_READ);
    if(result != FR_OK)
		return -1;
    
    while(1)
    {
        memset(str1, 0, sizeof(str1));
        memset(str2, 0, sizeof(str2));
		memset(buffer, 0, sizeof(buffer));
        
        buff = (char*)f_gets(buffer, sizeof(buffer), &fsrc);
        if(buff == NULL)
            break;
        sscanf(buff, "%[^'=']=%s", str1, str2);
		properties_add(c_prop, str1, strlen(str1), str2, strlen(str2));
    }
	result = f_close(&fsrc);
	
	return 0;
}

int8_t setting_save(container_prop_t *c_prop, const char* path)
{
	FRESULT result;
	FIL fsrc;
	uint8_t i;
	uint16_t size = 0;
	char  __buff[1000];
	UINT wsize;
	
	for(i = 0; i < c_prop->size; i++)
	{
		size += (c_prop->properties[i])->size_key;
		size += (c_prop->properties[i])->size_value;
		size += 4;
	}
	
	//__buff = malloc(size);
	//if(__buff == NULL)
	//	return -1;
	
	memset(__buff, 0, size);
    size = 0;
	for(i = 0; i < c_prop->size; i++)
	{
        
		sprintf(__buff + size, "%s= %s\n", (c_prop->properties[i])->key, (c_prop->properties[i])->value);
        size += (c_prop->properties[i])->size_key + (c_prop->properties[i])->size_value + 3;
	}
	
	result = f_open(&fsrc, path, FA_WRITE);
    if(result != FR_OK)
		return -1;
	
    //f_lseek(&fsrc,size);
	result = f_write(&fsrc, __buff, size, &wsize);
	if(result != FR_OK)
	{
		f_close(&fsrc);
		return -1;
	}
    
	result = f_sync(&fsrc);
    result = f_sync(&fsrc);
	result = f_close(&fsrc);
    return 0;
}