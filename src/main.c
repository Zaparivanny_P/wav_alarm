#include "stm32f10x.h"
#include "sdcard.h"
#include "board.h"
#include "engine.h"
#include "ff.h"
#include "codec.h"
#include "setting.h"
#include "string.h"

FATFS FATFS_Obj;

uint32_t g_time;

uint16_t play_time, _play_time = 600;

void sm_channnel1_init();
void sm_channnel1_play();
void sm_channnel1_wait();
void (*sm_channel1)() = sm_channnel1_init;


void sm_channnel2_init();
void sm_channnel2_play();
void sm_channnel2_wait();
void (*sm_channel2)() = sm_channnel2_init;

void init_tim7()
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

    NVIC_InitStruct.NVIC_IRQChannel = TIM1_UP_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStruct);
    NVIC_SetPriority(TIM1_UP_IRQn, NVIC_EncodePriority(4, 2, 0));
            
    TIM_TimeBaseInitStruct.TIM_Prescaler = 7200;
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Down;
    TIM_TimeBaseInitStruct.TIM_Period = 10000;
    TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
    TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseInitStruct);
    TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);
    //TIM_ARRPreloadConfig(TIM1, ENABLE);
    TIM_Cmd(TIM1, ENABLE);
}


void TIM1_UP_TIM16_IRQHandler()
{
    board_led_toggle();
    g_time++;
    TIM_ClearFlag(TIM1, TIM_FLAG_Update);
}

void sm_channnel1_init()
{
	if(!board_status_input_2())
	{
		return;
	}
    
    play_time = g_time + _play_time;
	sm_channel1 = sm_channnel1_play;
}

void sm_channnel1_play()
{
	
    if(play_time > g_time)
    {
        if(!get_channel_flag(CODEC_CHANNEL_6))
        {
            codec_load(CODEC_CHANNEL_6, "0:/sound_1.wav");
            codec_play(CODEC_CHANNEL_6);
        }
    }
	else
	{
		codec_disable_channel(CODEC_CHANNEL_6);
		codec_stop(CODEC_CHANNEL_6);
		sm_channel1 = sm_channnel1_wait;
	}
    
}

void sm_channnel1_wait()
{
	
}


void sm_channnel2_init()
{
	if(!board_status_input_1())
	{
		return;
	}
    
    play_time = g_time + _play_time;
	sm_channel2 = sm_channnel2_play;
	codec_load(CODEC_CHANNEL_5, "0:/sound_2.wav");
	codec_play(CODEC_CHANNEL_5);
}

void sm_channnel2_play()
{

	if(!get_channel_flag(CODEC_CHANNEL_5))
	{
		codec_disable_channel(CODEC_CHANNEL_5);
		codec_stop(CODEC_CHANNEL_5);
		sm_channel2 = sm_channnel2_wait;
	}

}

void sm_channnel2_wait()
{
	
}

void main()
{
    static FRESULT result;

    GPIO_InitTypeDef GPIO_InitStruct;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
    uint16_t cntt = 0;
    while(--cntt);
    sdcard_init();
    
    result = f_mount(&FATFS_Obj, "0", 1);
    if(result != FR_OK)
    {
        while(1)
        {
            result = f_mount(&FATFS_Obj, "0", 1);
            if(result == FR_OK)
                break;
        }
    }

    board_init();
    init_tim7();
    board_led_on();

	engine_init();
    codec_init();
    
    /*container_prop_t g_prop;
    memset(&g_prop, 0, sizeof(container_prop_t));
    setting_load(&g_prop, "0:configure.ini");
    
    uint16_t delay = 0;
    uint16_t _play_time = 0;
    
	setting_get_properties_to_int(&g_prop, "DELAY_S", &delay);
    setting_get_properties_to_int(&g_prop, "PLAY_TIME_S", &_play_time);
    if(_play_time == 0)
        _play_time = 600;*/

    
    while((board_status_input_1()) || (board_status_input_2()));
    

    

    while(1)
    {
		sm_channel1();
		sm_channel2();
		codec_update();
        engine_update(CODEC_CHANNEL_6);
    }

}