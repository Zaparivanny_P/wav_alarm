#include "board.h"


void board_input_init()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = _PIN(PIN_IN_1) | _PIN(PIN_IN_2);
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(_PORT(PORT_IN), &GPIO_InitStructure);
}

void board_out_init()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = _PIN(PIN_OUT_1) | _PIN(PIN_OUT_2);
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(_PORT(PORT_OUT), &GPIO_InitStructure);
}

void board_led_init()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = _PIN(PIN_LED_1);
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(_PORT(PORT_LED_1), &GPIO_InitStructure);
}

void board_init()
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	AFIO->MAPR |= AFIO_MAPR_SWJ_CFG_JTAGDISABLE;
    
	board_led_init();
	board_input_init();
	board_out_init();
}

void board_led_on()
{
	GPIO_SetBits(_PORT(PORT_LED_1), _PIN(PIN_LED_1));
}

void board_led_off()
{
	GPIO_ResetBits(_PORT(PORT_LED_1), _PIN(PIN_LED_1));
}

void board_led_toggle()
{
    _PORT(PORT_LED_1)->ODR ^= (1 << PIN_LED_1);
}

uint8_t board_status_input_1()
{
    return GPIO_ReadInputDataBit(_PORT(PORT_IN), _PIN(PIN_IN_1));
}

uint8_t board_status_input_2()
{
    return GPIO_ReadInputDataBit(_PORT(PORT_IN), _PIN(PIN_IN_2));
}