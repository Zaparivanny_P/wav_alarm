#ifndef SETTING_H
#define SETTING_H
#include "stm32f10x.h"

typedef struct properties_t
{
    char* key;
    char* value;
	uint8_t size_key;
	uint8_t size_value;
}properties_t;

typedef struct container_prop_t
{
	properties_t **properties;
	uint8_t size;
}container_prop_t;

int8_t setting_load(container_prop_t *c_prop, const char*);
int8_t setting_save(container_prop_t *c_prop, const char*);

int8_t setting_set_properties_u16(container_prop_t *c_prop, const char *key, uint16_t value);
int8_t setting_get_properties(container_prop_t *c_prop, const char *key, char* value);

int8_t setting_get_properties_to_int(container_prop_t *c_prop, const char *key, uint16_t *value);

#endif