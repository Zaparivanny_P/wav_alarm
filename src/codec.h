#ifndef CODEC_H
#define CODEC_H
#include "stm32f10x.h"

#define CODEC_BUFER_SIZE (1024)
#define AMOUNT_CHANNEL 6


#define CODEC_TIM_RCC_PeriphClockCmd 	RCC_APB1PeriphClockCmd
#define CODEC_TIM_RCC_Periph_TIMX    	RCC_APB1Periph_TIM3
#define CODEC_NVIC_IRQTIMChannel	 	TIM3_IRQn
#define CODEC_TIMX						TIM3
#define CODEC_TIMX_IRQHandler			TIM3_IRQHandler


typedef enum CODEC_STATUS
{
	CODEC_OK 			= 0x0,
	CODEC_ERROR_MALLOC  = 0x1,
	CODEC_ERROR_PATH	= 0x2,
	CODEC_ERROR_READ	= 0x3,
	CODEC_ERROR_WAV 	= 0x4,
	
}Codec_Status;

enum codec_channel
{
	CODEC_CHANNEL_1 	= 0x0,
	CODEC_CHANNEL_2 	= 0x1,
	CODEC_CHANNEL_3 	= 0x2,
	CODEC_CHANNEL_4 	= 0x3,
	CODEC_CHANNEL_5 	= 0x4,
	CODEC_CHANNEL_6 	= 0x5,
	CODEC_CHANNEL_ALL 	= 0x6,
};

struct wav
{
	uint32_t chunkId;
	uint32_t chunkSize;
	uint32_t format;
	uint32_t subchunk1Id;
	uint32_t subchunk1Size;
	uint16_t audioFormat;
	uint16_t numChannels;
	uint32_t sampleRate;
	uint32_t byteRate;
	uint16_t blockAlign;
	uint16_t bitsPerSample;
	uint32_t subchunk2Id;
	uint32_t subchunk2Size;
};

Codec_Status codec_init();
Codec_Status codec_load(enum codec_channel channel, const char* path);
int codec_play(enum codec_channel channel);
int codec_stop(enum codec_channel channel);
void codec_free(enum codec_channel channel);
void codec_update();
void codec_deinit();


/*********private function***********/
uint8_t get_channel_flag(enum codec_channel _channel);
void codec_enable_channel(enum codec_channel _channel);
void codec_disable_channel(enum codec_channel _channel);

#endif
