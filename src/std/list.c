#include "list.h"
#include "string.h"
#include "stdlib.h"

uint8_t list_push_back(List *list, void* ptr, uint8_t size)
{
	void *p = malloc(size);
	if(p == NULL && size != 0)
	{
		return -1;
	}
	
	ListItem *l = malloc(sizeof(ListItem));
	if(l == NULL)
	{
		return -1;
	}
	l->item = p;
	memcpy(p, ptr, size);
    if(list->current == NULL)
    {
        list->current = l;
        l->prew = NULL;
    }
    else
    {
		if(list->current->next != NULL)
		{
			list_iterator_end(list);
		}
        list->current->next = l;
        l->prew = list->current;
        list->current = l;
    }
	l->next = NULL;
	list->size++;
	return 0;
}

uint8_t list_iterator_next(List *list)
{
	ListItem *item = list->current;
	if(item->next == NULL)
	{
		return -1;
	}
	list->current = item->next;
	return 0;
}

uint8_t list_iterator_prew(List *list)
{
	ListItem *item = list->current;
	if(item->prew == NULL)
	{
		return -1;
	}
	list->current = item->prew;
	return 0;
}

void *list_get(List *list)
{
	return list->current->item;
}

void list_iterator_begin(List *list)
{
	while(!list_iterator_prew(list));
}
void list_iterator_end(List *list)
{
	while(!list_iterator_next(list));
}

void list_erase(List *list)
{
	ListItem li_current = list->current;
	ListItem li_next = list->current->next;
	ListItem li_prew = list->current->prew;
	li_prew->next = li_next;
	li_next->prew = li_prew;
	free(list->current->item);
	free(list->current);
	if(li_next != NULL)
		list->current = li_next;
	else
		list->current = li_prew;
}

void list_free(List *list)
{
    if(list->current == NULL)
        return;
	list_iterator_end(list);
	while(1)
	{
		ListItem *l = list->current;
		if(l->prew == NULL)
		{
			break;
		}
		list->current = l->prew;
		free(l->item);
		free(l);
	}
}