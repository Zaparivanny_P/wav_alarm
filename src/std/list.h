#ifndef LIST_H
#define LIST_H

#include "stdint.h"

struct ListItem;

typedef struct ListItem
{
	struct ListItem *prew;
	struct ListItem *next;
	void *item;
}ListItem;

typedef struct List
{
	ListItem *current;
	uint8_t size;
}List;

/*private function*/




/*public function*/
uint8_t list_push_back(List *list, void* ptr, uint8_t size);
void *list_get(List *list);

uint8_t list_iterator_next(List *list);
uint8_t list_iterator_prew(List *list);
void list_iterator_begin(List *list);
void list_iterator_end(List *list);
void list_erase(List *list);

static inline uint8_t list_size(List *list)
{
	return list->size;
}

void list_free(List *list);

#endif