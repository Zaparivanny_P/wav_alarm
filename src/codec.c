#include "codec.h"
#include "ff.h"
#include "diskio.h"
#include "string.h"
#include "stdlib.h"
// 1 2 3 4 6 7 8

enum CODEC_SM
{
	CODEC_LOAD_1,
	CODEC_LOAD_2,
	CODEC_WAIT_1,
	CODEC_WAIT_2,
	CODEC_WAIT__,
	CODEC_STOP,
};

enum CODEC_SM csm[AMOUNT_CHANNEL];// = {CODEC_WAIT__, CODEC_WAIT__, CODEC_WAIT__, CODEC_WAIT__, CODEC_WAIT__};
//uint8_t codec_buff_channel_1[CODEC_BUFER_SIZE];
//uint8_t codec_buff_channel_2[CODEC_BUFER_SIZE];
uint8_t* codec_pchannels[AMOUNT_CHANNEL];

FIL codec_file[AMOUNT_CHANNEL];
FRESULT result;
struct wav wav_header;

uint32_t codec_count_ch[AMOUNT_CHANNEL];
uint8_t codec_channel_enabled;

extern FATFS FATFS_Obj;

void codec_init_tim()
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_OCInitTypeDef TIM_OCInitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	GPIO_InitTypeDef GPIO_InitStruct;
	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_0 ;//GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	
	CODEC_TIM_RCC_PeriphClockCmd(CODEC_TIM_RCC_Periph_TIMX, ENABLE);
	
	NVIC_InitStruct.NVIC_IRQChannel = CODEC_NVIC_IRQTIMChannel;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
	NVIC_SetPriority(CODEC_NVIC_IRQTIMChannel, NVIC_EncodePriority(4, 2, 0));
		
	TIM_TimeBaseInitStruct.TIM_Prescaler = 4;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Down;
	TIM_TimeBaseInitStruct.TIM_Period = 0x00FF;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(CODEC_TIMX, &TIM_TimeBaseInitStruct);
	//TIM_ARRPreloadConfig(TIM2, ENABLE);
	TIM_ITConfig(CODEC_TIMX, TIM_IT_Update, ENABLE);


  
	TIM_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_OutputNState = TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse = 0;
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStruct.TIM_OCNPolarity = TIM_OCNPolarity_High;
	TIM_OCInitStruct.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM_OCInitStruct.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	
	TIM_OC1Init(CODEC_TIMX, &TIM_OCInitStruct);
	TIM_OC2Init(CODEC_TIMX, &TIM_OCInitStruct);
	TIM_OC3Init(CODEC_TIMX, &TIM_OCInitStruct);
	TIM_OC4Init(CODEC_TIMX, &TIM_OCInitStruct);
    CODEC_TIMX->CCR1 = 0xff;
    CODEC_TIMX->CCR2 = 0xff;
    CODEC_TIMX->CCR3 = 0xff;
    CODEC_TIMX->CCR4 = 0xff;
	TIM_Cmd(CODEC_TIMX, ENABLE);
}

Codec_Status codec_init()
{
	codec_init_tim();
	
	for(uint8_t i = 0; i < AMOUNT_CHANNEL; i++)
	{
		codec_pchannels[i] = (uint8_t *)malloc(CODEC_BUFER_SIZE * sizeof(uint8_t));
		if(codec_pchannels[i] == NULL)
		{
			for(;i >0; --i)
				free(codec_pchannels[i]);
			return CODEC_ERROR_MALLOC;
		}
		csm[i] = CODEC_WAIT__;
	}
	return CODEC_OK;
}

void codec_deinit()
{
	for(uint8_t i = 0; i < AMOUNT_CHANNEL; i++)
	{
		free(codec_pchannels[i]);
	}
}

void CODEC_TIMX_IRQHandler()
{
	TIM_ClearFlag(CODEC_TIMX, TIM_FLAG_Update);
	/*
	if(codec_channel_enabled & 0x10) // all channel
	{
		TIM5->CCR1 = codec_channel1_buff[codec_count_ch[4]];
		TIM5->CCR2 = codec_channel1_buff[codec_count_ch[4]];
		TIM5->CCR3 = codec_channel1_buff[codec_count_ch[4]];
		TIM5->CCR4 = codec_channel1_buff[codec_count_ch[4]];
		if(++codec_count_ch[4] >= sizeof(codec_channel1_buff)) codec_count_ch[4] = 0;
		return;
	}*/
	
	if(get_channel_flag(CODEC_CHANNEL_3))
	{
		CODEC_TIMX->CCR1 = codec_pchannels[CODEC_CHANNEL_3][codec_count_ch[CODEC_CHANNEL_3]];
		if(++codec_count_ch[CODEC_CHANNEL_3] >= CODEC_BUFER_SIZE) codec_count_ch[CODEC_CHANNEL_3] = 0;
	}
	if(get_channel_flag(CODEC_CHANNEL_4))
	{
		CODEC_TIMX->CCR2 = codec_pchannels[CODEC_CHANNEL_4][codec_count_ch[CODEC_CHANNEL_4]];
		if(++codec_count_ch[CODEC_CHANNEL_4] >= CODEC_BUFER_SIZE) codec_count_ch[CODEC_CHANNEL_4] = 0;
	}
	if(get_channel_flag(CODEC_CHANNEL_5))
	{
		CODEC_TIMX->CCR3 = codec_pchannels[CODEC_CHANNEL_5][codec_count_ch[CODEC_CHANNEL_5]] / 2;
		if(++codec_count_ch[CODEC_CHANNEL_5] >= CODEC_BUFER_SIZE) codec_count_ch[CODEC_CHANNEL_5] = 0;
	}
	if(get_channel_flag(CODEC_CHANNEL_6))
	{
		CODEC_TIMX->CCR4 = codec_pchannels[CODEC_CHANNEL_6][codec_count_ch[CODEC_CHANNEL_6]];
		if(++codec_count_ch[CODEC_CHANNEL_6] >= CODEC_BUFER_SIZE) codec_count_ch[CODEC_CHANNEL_6] = 0;
	}
}

void codec_update()
{
	UINT n_read = 1;

	static char _count_channel = 0;
	if(!codec_channel_enabled) return;
	
	if(++_count_channel > AMOUNT_CHANNEL) _count_channel = 0;
	if(!((codec_channel_enabled >> _count_channel) & 0x1)) return;

	switch(csm[_count_channel])
	{
		case CODEC_LOAD_1:
			f_read(&codec_file[(int)_count_channel], codec_pchannels[_count_channel], CODEC_BUFER_SIZE / 2, &n_read);
			csm[_count_channel] = CODEC_WAIT_2;
			break;
		case CODEC_LOAD_2:
            f_read(&codec_file[(int)_count_channel], codec_pchannels[_count_channel] + CODEC_BUFER_SIZE / 2, CODEC_BUFER_SIZE / 2, &n_read);
			csm[_count_channel] = CODEC_WAIT_1;
			break;
		case CODEC_WAIT_1:
			if(codec_count_ch[_count_channel] >= (CODEC_BUFER_SIZE / 2)) 
				csm[_count_channel] = CODEC_LOAD_1;
			break;
		case CODEC_WAIT_2:
			if(codec_count_ch[_count_channel] < (CODEC_BUFER_SIZE / 2)) 
				csm[_count_channel] = CODEC_LOAD_2;
			break;
		case CODEC_STOP:
			csm[_count_channel] = CODEC_WAIT__;
			codec_stop((enum codec_channel)_count_channel);
			codec_free((enum codec_channel)_count_channel);
		case CODEC_WAIT__:
			break;
		default:
			csm[_count_channel] = CODEC_WAIT__;
			codec_stop((enum codec_channel)_count_channel);
			codec_free((enum codec_channel)_count_channel);
			break;
	}
	if(n_read == 0)
	{
		csm[_count_channel] = CODEC_STOP; 
	}
}

Codec_Status codec_load(enum codec_channel channel, const char* path)
{
	UINT nRead;
	if(path == NULL) return CODEC_ERROR_PATH;
	
	codec_stop(channel);
	codec_free(channel);
	result = f_open(&codec_file[(int)channel], path, FA_OPEN_EXISTING | FA_READ);
    if (result != FR_OK)
    {
		return CODEC_ERROR_READ;
    }
	f_read(&codec_file[(int)channel], &wav_header, sizeof(struct wav), &nRead);
	
	if(wav_header.chunkId != 0x46464952) return CODEC_ERROR_WAV;
	if(wav_header.format != 0x45564157) return CODEC_ERROR_WAV;
	if(wav_header.subchunk1Id != 0x20746D66) return CODEC_ERROR_WAV;
	
	CODEC_TIMX->PSC = 72000000 / 256 / wav_header.sampleRate - 1;

	
	codec_count_ch[(int)channel] = 0;
    csm[(int)channel] = CODEC_WAIT_2;
	//codec_channel_enabled |= (1 << (int)channel);
	codec_enable_channel(channel);
	f_read(&codec_file[(int)channel], codec_pchannels[(int)channel], CODEC_BUFER_SIZE / 2, &nRead);
	
	return CODEC_OK;
}


int codec_play(enum codec_channel channel)
{
	switch(channel)
	{
		case CODEC_CHANNEL_3:
		case CODEC_CHANNEL_4:
		case CODEC_CHANNEL_5:
		case CODEC_CHANNEL_6: TIM_Cmd(CODEC_TIMX, ENABLE); break;
		case CODEC_CHANNEL_ALL:
								TIM_Cmd(CODEC_TIMX, ENABLE); break;
	}
	return 0;	
}

int codec_stop(enum codec_channel channel)
{
    switch(channel)
	{
        case CODEC_CHANNEL_3: CODEC_TIMX->CCR1 = 0xff; break;
        case CODEC_CHANNEL_4: CODEC_TIMX->CCR2 = 0xff; break;
        case CODEC_CHANNEL_5: CODEC_TIMX->CCR3 = 0xff; break;
        case CODEC_CHANNEL_6: CODEC_TIMX->CCR4 = 0xff; break;
    }
    return 0;
}
/*
int codec_stop(enum codec_channel channel)
{
	switch(channel)
	{
		case CODEC_CHANNEL_1: TIM_Cmd(TIM2, DISABLE); break;
		case CODEC_CHANNEL_2: TIM_Cmd(TIM1, DISABLE); break;
		case CODEC_CHANNEL_ALL: TIM_Cmd(TIM1, DISABLE); 
								TIM_Cmd(TIM2, DISABLE); 
								TIM_Cmd(TIM5, DISABLE); break;
	}
	return 0;
}
*/

void codec_free(enum codec_channel channel)
{
	//codec_channel_enabled &= ~(0x1 << (int)channel);
	codec_disable_channel(channel);
	f_close(&codec_file[channel]);
}

inline uint8_t get_channel_flag(enum codec_channel _channel)
{
	return codec_channel_enabled & (1 << (int)_channel);
}

inline void codec_enable_channel(enum codec_channel _channel)
{
	codec_channel_enabled |= (1 << (int)_channel);
}

inline void codec_disable_channel(enum codec_channel _channel)
{
	codec_channel_enabled &= ~(0x1 << (int)_channel);
}
