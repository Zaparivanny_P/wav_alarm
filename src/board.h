#ifndef BOARD_H
#define BOARD_H

#include "stm32f10x.h"


#define __PIN(a) GPIO_Pin_ ## a
#define _PIN(a) __PIN(a)

#define __PORT(a) GPIO ## a
#define _PORT(a) __PORT(a)

#define BOARD_REV 2

#if BOARD_REV == 1
/*Input*/
#define PIN_IN_1 5
#define PIN_IN_2 6
#define PORT_IN B
/*Out*/
#define PIN_OUT_1 0
#define PIN_OUT_2 1
#define PORT_OUT B
/*LED*/
#define PIN_LED_1 4
#define PORT_LED_1 B
#elif BOARD_REV == 2
/*Input*/
#define PIN_IN_1 4
#define PIN_IN_2 3
#define PORT_IN B
/*Out*/
#define PIN_OUT_1 0
#define PIN_OUT_2 1
#define PORT_OUT B
/*LED*/
#define PIN_LED_1 5
#define PORT_LED_1 B
#endif


void board_init();
void board_led_on();
void board_led_off();
void board_led_toggle();
uint8_t board_status_input_1();
uint8_t board_status_input_2();

#endif